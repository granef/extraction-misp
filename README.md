<img  src="https://is.muni.cz/www/milan.cermak/granef/granef-logo.svg"  height="60px">

[**Graph-Based Network Forensics**](https://gitlab.ics.muni.cz/granef/granef)**: MISP threat sharing extraction**

---

The Extraction-MISP module connects to the defined instance of the [MISP Open Source Threat Intelligence Platform](https://www.misp-project.org/) and downloads all events within the specified time window. MISP events are stored as `jsonl` file in the following format:

```
 {
    "id": <event id>,
    "url": <url to the event details>,
    "info": <event information>,
    "timestamp": <event timestamp>,
    "related": [<related event id>],
    "iocs": [{
        "type": <attribute type>,
        "category": <attribtue category>,
        "timestamp": <attribute timestamp>,
        "value": <attribute value>
    }, ...]
}
```


### Requirements

- Available MISP instance
- Docker 
- Python3
- Python3 packages in [requirements.txt](requirements.txt)

The installation can be performed using the following command:

```bash
$ git clone https://gitlab.ics.muni.cz/granef/extraction-misp.git
```

Use the following command to build the Docker container:

```bash
$ docker build --tag=granef/extraction-misp --pull .
```


### Usage

The Docker container can be either run separately with command line arguments or as part of the Granef toolkit with arguments set in the [granef.yml](https://gitlab.ics.muni.cz/granef/granef/-/blob/master/granef.yml) configuration file. 

The following arguments can be set:

| Short argument | Long argument | Description | Default | Required |
|-|-|-|-|-|
|`-u`|`--url`|URL to MISP instance|–|T|
|`-k`|`--key`|Automation authentication key of user|–|T|
|`-df`|`--date_from`|Date from which to select MISP events|One month from today|F|
|`-dt`|`--date_to`|Date by which to select MISP events|Today|F|
|`-o`|`--output`|Output data directory path|`granef-extraction/misp/`|F|
|`-m`|`--mounted`|Mounted data directory path|`/data/`|F|
|`-l`|`--log`|Log level (possible values: `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`)|`INFO`|F|

Use the following command to start the extraction:

```bash
$ docker run --rm -v <LOCAL_DIR>:<MOUNTED> granef/extraction-misp -m <MOUNTED> -u <MISP_URL> -k <MISP_KEY>
```

Or with the default `--mounted` path:

```bash
$ docker run --rm -v <LOCAL_DIR>:/data/ granef/extraction-misp -i <INPUT_DIR_OR_FILE_PATH> -u <MISP_URL> -k <MISP_KEY>
```

The Extraction-MISP module then performs the following steps:

1. Connects to the MISP instance using provided key.
2. Obtains all events within the specified time window.
3. Extracts events information and relevant IoC and stores them as jsonl file.
4. Saves the generated jsonl files to a local output directory.
