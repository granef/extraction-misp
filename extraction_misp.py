#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2021  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""Application to extract events from the MISP platform.

This script export all events in the specified data range, extract IoCs, and store them in given output directory as file
named "events-<date_from>-<date_to>.jsonl file.
"""


from datetime import date       # Date parsing
from dateutil.relativedelta import relativedelta        # Time subtraction support
import sys          # Common system functions
import os           # Common os functions
import argparse     # Arguments parser
import logging, coloredlogs
from typing import List         # Standard logging functionality with colours functionality
import json         # JSON format processing
from pymisp import ExpandedPyMISP
from pymisp.mispevent import MISPAttribute, MISPEvent   # MISP platform connection


# Capture and log pymisp module warnings
logging.captureWarnings(True)


def reduce_attribute(attribute: MISPAttribute) -> dict:
    """Select relevant info from the MISP event attribute.

    Args:
        attribute (MISPAttribute): MISP attribute object from MISP event.

    Returns:
        dict: Dictionary with type, category, timestamp, and value keys.
    """
    return {
        "type": attribute.type,
        "category": attribute.category,
        "timestamp": attribute.timestamp.strftime('%Y-%m-%dT%H:%M:%SZ'),
        "value": attribute.value.strftime('%Y-%m-%dT%H:%M:%SZ') if attribute.type == "datetime" else attribute.value
    }


def extract_iocs(misp_events: List[MISPEvent], misp_url: str) -> list:
    """Extract IoCs from MISP events.

    The function returns a list of dictionaries with information relevant to the event and its IoCs. IoCs are extracted
    from all event attributes (attributes as well as Objects attributes).
    
    Structure of extracted event with IoCs:
    {
        "id": <event id>,
        "url": <url to the event details>,
        "info": <event information>,
        "timestamp": <event timestamp>,
        "related": [<related event id>],
        "iocs": [{
            "type": <attribute type>,
            "category": <attribtue category>,
            "timestamp": <attribute timestamp>,
            "value": <attribute value>
        }, ...]
    }

    Args:
        misp_events (List[MISPEvent]): List of MISP event objects.
        misp_url (str): MISP instance URL

    Returns:
        list: List of dictionaries with extracted information from the event.
    """
    events_iocs = []
    for misp_event in misp_events:
        event_ioc = {
            "id": misp_event.id,
            "url": misp_url + "/events/view/" + str(misp_event.id),
            "info": misp_event.info,
            "timestamp": misp_event.timestamp.strftime('%Y-%m-%dT%H:%M:%SZ'),
            "related": [related_event["Event"].id for related_event in misp_event.related_events],
            "iocs" : [reduce_attribute(event_attribute) for event_attribute in misp_event.attributes]
        }
        for event_object in misp_event.Object:
            for object_attribute in event_object.attributes:
                event_ioc["iocs"].append(reduce_attribute(object_attribute))          
        events_iocs.append(event_ioc)
    return events_iocs


if __name__ == "__main__":
    # Define application arguments (automatically creates -h argument)
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--url", help="URL to MISP instance", required=True, type=str)
    parser.add_argument("-k", "--key", help="Automation authentication key of user", required=True, type=str)
    parser.add_argument("-l", "--log", choices=["debug", "info", "warning", "error", "critical"], help="Log level", required=False, default="INFO", type=str)
    parser.add_argument('-o', '--output', help='Output data directory path', default='granef-extraction/misp/', type=str)
    parser.add_argument('-m', '--mounted', help='Mounted data directory path', default='/data/', type=str)
    parser.add_argument("-df", "--date_from", help="Date from which to select MISP events (default month ago)",
        default=date.today() - relativedelta(months=1), type=date.fromisoformat)
    parser.add_argument("-dt", "--date_to", help="Date by which to select MISP events (default today)",
        default=date.today(), type=date.fromisoformat)
    args = parser.parse_args()

    # Set logging
    coloredlogs.install(level=getattr(logging, args.log.upper()), fmt="%(asctime)s [%(levelname)s]: %(message)s")

    # Initialize connection to MISP instance with proper URL
    misp_url = args.url if args.url.startswith("http") else "https://" + args.url
    try:
        misp_inst = ExpandedPyMISP(misp_url, args.key, False)
    except Exception as e:
        logging.error("Error occurred while trying to connect to the MISP instance!")
        sys.exit(1)
    
    # Get all MISP events observed in the specified date range
    date_from = args.date_from.strftime("%Y-%m-%d")
    date_to = args.date_to.strftime("%Y-%m-%d")
    misp_search_result = misp_inst.search(date_from=date_from, date_to=date_to, pythonify=True)

    # Extract IoCs and store them in the output directory
    if misp_search_result:
        # Create output directory if not exists
        output_directory = args.mounted + '/' + args.output + '/'
        logging.debug('Arguments set to: output = ' + output_directory)
        if not os.path.isdir(output_directory):
            os.makedirs(output_directory)  # TODO Currently overwrites whatever files are in this directory, if it already exists.
        
        # Store IoCs as separate objects (JSONL format)
        output_file_name = "events-{date_from}-{date_to}.jsonl".format(date_from=args.date_from.strftime("%Y%m%d"), date_to=args.date_to.strftime("%Y%m%d"))
        with open(output_directory + output_file_name, 'w') as output_file:
            for ioc in extract_iocs(misp_search_result, misp_url):
                json.dump(ioc, output_file)   
                output_file.write('\n')   
        logging.info("All MISP events succesfully exported to {output_file_path}".format(output_file_path=args.output + '/' + output_file_name))
    else:
        logging.warning("No MISP events were found within the specified date range: {date_from} to {date_to}".format(date_from=date_from, date_to=date_to))
